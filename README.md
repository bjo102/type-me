# TypeMe

A package for typing animations.

## TypMe objects

- Specifying target DOM elements.
- Stashing **Content** - the array of strings to print in sequence.
- Methods for animating the adding of characters to the DOM element.

## TypeGroup objects

- Grouping TypeMe objects together, making them work in harmony.
