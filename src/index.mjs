import sleep from './helpers/sleep.mjs';
import TypeMe from './modules/TypeMe.mjs';
import TypeGroup from  './modules/TypeGroup.mjs';

const sequentialTypeGroup = new TypeGroup([
  new TypeMe(
    document.querySelector('#p-1a'),
    [
      'This is a type me object...',
      "This is the second content string..."
    ],
    {
      typeSpeed: 60,
      displayTime: 3000,
      opacityTrans: 300
    }
  ),
  new TypeMe(
    document.querySelector('#p-1b'),
    [
      'And this is another type me object...',
      "Lorem ipsum a dolores saepe quia voluptatem quia..."
    ],
    {
      typeSpeed: 60,
      displayTime: 3000,
      opacityTrans: 300
    }
  ),
  new TypeMe(
    document.querySelector('#p-1c'),
    [
      'And this is another type me object...',
      "Lorem ipsum a dolores saepe quia occaecati voluptatem quia..."
    ],
    {
      typeSpeed: 60,
      displayTime: 3000,
      opacityTrans: 300
    }
  )
])

const simultaneousTypeGroup = new TypeGroup([
  new TypeMe(
    document.querySelector('#p-2a'),
    [
      'This is a type me object...',
      "This is the second content string..."
    ],
    {
      typeSpeed: 60,
      displayTime: 3000,
      opacityTrans: 300
    }
  ),
  new TypeMe(
    document.querySelector('#p-2b'),
    [
      'And this is another type me object...',
      "Lorem ipsum a dolores saepe quia voluptatem quia...",
      "labore veritatis sit sit harum. Perferendis numqu..."
    ],
    {
      typeSpeed: 60,
      displayTime: 3000,
      opacityTrans: 300
    }
  )
]);


function sqLoop() {
  sequentialTypeGroup.sequenceType()
  .then(() => sleep(2000))
  .then(() => sequentialTypeGroup.hideStrings())
  .then(() => sequentialTypeGroup.clearStrings())
  .then(() => sequentialTypeGroup.showStrings())
  .then(() => {
    sequentialTypeGroup.incContentIndexInc();
    sqLoop();
  })
}

function siLoop() {
  simultaneousTypeGroup.simultaneousType()
  .then(() => sleep(2000))
  .then(() => simultaneousTypeGroup.hideStrings())
  .then(() => simultaneousTypeGroup.clearStrings())
  .then(() => simultaneousTypeGroup.showStrings())
  .then(() => {
    simultaneousTypeGroup.incContentIndexInc();
    siLoop();
  })
}

sqLoop();
siLoop();