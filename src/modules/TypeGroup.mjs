/**
 * TypeMe Modules: TypeMe
 * 
 * @since 1.0.0
 * 
 * @package TypeMe\Scripts
 */

import getHighestNum from "../helpers/getHighestNum.mjs";

/**
 * The TypeGroup objects for harmonising TypeMe objects.
 * 
 * @since 1.0.0
 * 
 * @param {array} group TypeMe objects.
 */
const TypeGroup = class {

    constructor(group) {
        this.typeGroup = group;
        this.contentIndex = 0;
        this.highestIndex = getHighestNum(...this.typeGroup.map(typeMe => typeMe.content.length));
    }

    write() {
        while(this.contentIndex < this.highestIndex) {
          this.contentIndex ++
        }
    }
    
    sequenceType() {
        return new Promise( async (resolve) => {
            for (let typeMe of this.typeGroup) {
                await typeMe.typeString(this.contentIndex);
            }
            resolve();
        });
    }
    
    simultaneousType() {
        return Promise.all(
            this.typeGroup.map(typeMe => typeMe.typeString(this.contentIndex))
        )
    }

    incContentIndexInc() {
        if(this.contentIndex === (this.highestIndex - 1)) {
            this.contentIndex = 0; 
        } else {
            this.contentIndex ++;
        }
    }

    hideStrings() {
        return Promise.all(
            this.typeGroup.map(typeMe => typeMe.hideString())
        )
    }

    showStrings() {
        return Promise.all(
            this.typeGroup.map(typeMe => typeMe.showString())
        )
    }

    clearStrings() {
        return Promise.all(
            this.typeGroup.map(typeMe => typeMe.clearString())
        )
    }
    
}

export default TypeGroup;