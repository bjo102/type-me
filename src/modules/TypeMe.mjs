/**
 * TypeMe Modules: TypeMe
 * 
 * @since 1.0.0
 * 
 * @package TypeMe\Scripts
 */

import sleep from "../helpers/sleep.mjs";

/**
 * The TypeMe objects containing content arrays and typing animations logic.
 * 
 * @since 1.0.0
 * 
 * @param {object} element The DOM element to target for animations. 
 * @param {array} content The content strings to fill the element with.
 * @param {object} settings The specifications for how animations will run for this object.
 */
const TypeMe = class {

    constructor(element, content, settings) {
        this.element        = element;
        this.content        = content;
        this.typeSpeed      = settings.typeSpeed ? settings.typeSpeed : 10;
        this.displayTime    = settings.displayTime ? settings.displayTime : 10;
        this.opacityTrans   = settings.opacityTrans ? settings.opacityTrans : 300;

        this.element.style.transition = `all ${(settings.opacityTrans / 1000)}s`;
    }

    typeString(i = 0) {
        const fullString = this.getContentString(i);
        const typeString = new Promise((resolve) => {
            let i = 0;
            let typeMain = setInterval(() => {
                this.element.innerHTML += fullString[i];
                let isWordFull = this.element.innerHTML === fullString;
                if (isWordFull) {
                    clearInterval(typeMain);
                    resolve('typed 102')
                }
                i++;
            }, this.typeSpeed);
        });

        return typeString
    }

    clearString() {
        return new Promise(resolve => {
            this.element.innerHTML = '';
            resolve();
        });
    }

    hideString() {
        return new Promise(resolve => {
            this.element.style.opacity = '0';
            sleep(this.opacityTrans)
            .then(() => resolve());
        });
    }

    showString() {
        return new Promise(resolve => {
            this.element.style.opacity = '1';
            sleep(this.opacityTrans)
            .then(() => resolve());
        });
    }

    getString() {
        return this.element.innerHTML;
    }

    getContentString(i) {
        let theIndex = 0;
        let endIndex = this.content.length - 1;

        if (i > endIndex) {
            theIndex = endIndex;
        } else if (i < 0) {
            theIndex = 0;
        } else {
            theIndex = i;
        }

        return this.content[theIndex];
    }

}

export default TypeMe;