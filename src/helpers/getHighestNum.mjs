/**
 * TypeMe Helpers: getHighestNum
 * 
 * @since 1.0.0
 * 
 * @package TypeMe\Helpers
 */

/**
 * Get the highest between all arguements (as many integers as you want).
 * 
 * @since 1.0.0
 * 
 * @param  {...int} ints The numbers to compare.
 * @returns {int} The highest number provided.
 */
const getHighestNum = (...ints) => Math.max(...ints)
export default getHighestNum