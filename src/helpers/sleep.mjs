/**
 * TypeMe Helpers: sleep
 * 
 * @since 1.0.0
 * 
 * @package TypeMe\Helpers
 */

/**
 * Delay the script.
 * 
 * Be sure to call the funciton like this: "await sleep"
 * 
 * @param {int} ms Milliseconds by which to delay the script.
 * @returns {Promise} The resolve of the waiting period.
 */
 const sleep = ms => new Promise(r => setTimeout(r, ms));

 export default sleep